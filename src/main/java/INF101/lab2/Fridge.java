package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
	ArrayList<FridgeItem> expiredItem = new ArrayList<FridgeItem>();

	
	public int nItemsInFridge() {
		// Teller items i listen
		int itemCount = items.size();
		return itemCount;
		
	}
	public int totalSize() {
		int totalSize = 20;
		return totalSize;
	}
	
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < 20) {
			items.add(item);
			return true;
		}
		else {
			return false;
		}
	}

	public void takeOut(FridgeItem item) {
		if (items.contains(item)) {
			items.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}
	}

	public void emptyFridge() {
		items.clear();
	}
		
		
	public List<FridgeItem> removeExpiredFood(){
		for(FridgeItem item : items) {
		    if(item.hasExpired() == true) 
		        expiredItem.add(item); 
		}
		return expiredItem;
	}
}
